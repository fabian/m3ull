# m3ull
Pass the URL of a M3U playlist to m3ull as the m3u-argument and it
returns a list of links to the playlist itself and all contained 
segments.

You can use this list as the input for wget, like so:
```
m3ull --m3u=http://video.storage.com/big_buck_bunny.mp4.m3u8 | \
    parallel --gnu "wget {}"
```
